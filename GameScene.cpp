#include "GameScene.h"
#include "ResourceManager.h"
#include "SoundPlayer.h"

bool GameScene::setUp() {
	add(StarsBackground::getInstance());
	m_currentState = Intro;

	m_paused = false;

	m_nave = (Sprite *)add(ResourceManager::getResource<Sprite>("sprite_nave"));

	m_nave->setPosition(-400, get_surf_height() / 2);

	m_controller = new TouchController();
	m_controller->bind(m_nave);

	m_pauseButton = (Sprite *) add(ResourceManager::getResource<Sprite>("sprite_pause"));
	m_pauseButton->setPosition(get_surf_width() - 120, 20);

	m_fcontroller = (FiringController *) add(new FiringController(
		ResourceManager::getResource<Animation>("anim_bullet")));
	m_fcontroller->bind(m_nave);

	m_spawner = (AlienSpawner *) add(new AlienSpawner( ResourceManager::getResource<Animation>("anim_alien"), 
		ResourceManager::getResource<Animation>("anim_bullet"), 3.5f));
	m_spawner->m_spawning = false;

	CollisionManager::Add(m_nave, "nave");
	m_expSprite = (Sprite *) add(ResourceManager::getResource<Sprite>("sprite_explosion"));
	m_expSprite->m_visible = false;

	m_setUp = true;

	subscribe("nave", "aliens");
	subscribe("nave", "alien_bullets");
	subscribe("bullets", "aliens");

	m_score = 0;
	m_scoreSprite = (ScoreSprite *) add(ResourceManager::getResource<ScoreSprite>("sprite_score"));
	m_scoreSprite->rebind(&m_score);

	return true;
}

void GameScene::reset() {
	m_currentState = Dying;
}

bool GameScene::tearDown() {

	m_fcontroller->unbind();
	m_controller->unbind();

	delete m_controller;
	delete m_fcontroller;

	delete m_spawner;
	m_setUp = false;

	return true;
}


void GameScene::update(float dt) {
	if (!m_paused) {
		defaultUpdate(dt);
		m_spawner->m_spawning = m_currentState == Playing;
		switch (m_currentState) {
			case Intro:
				m_nave->translate(160.0f * dt, 0.0f);
				if (m_nave->m_x >= 20) {
					m_currentState = Playing;
				}
				break;
			case Playing:
				CollisionManager::CheckCollisions();
				break;
			case Dying:
				if (m_expSprite->animationFinished()) {
					// auto-retry
					m_nave->m_updating = m_nave->m_visible = true;
					m_expSprite->m_updating = m_expSprite->m_visible = false;
					m_spawner->clear();
					m_fcontroller->clearBullets();
					m_nave->setPosition(-100, get_surf_height() / 2);
					m_currentState = Intro;
					m_score = 0;
				}
				break;
		}
	}	
}

void GameScene::render() {
	defaultRender();
	if (m_paused) {
		CIwColour c = Iw2DGetColour();
		Iw2DSetColour(0x77000000);
		Iw2DFillRect(CIwFVec2(0, 0), CIwFVec2(Iw2DGetSurfaceWidth(), Iw2DGetSurfaceHeight()));
		Iw2DSetColour(c);
	}
}


void GameScene::key(KeyState ks, s3eKey key) {
	switch (m_currentState) {
		case Playing:
			if (!m_paused) {
				m_controller->key(ks, key);
				m_fcontroller->key(ks, key);
			}
			if (ks == EventKey::Pressed && (key == s3eKeyBack)) {
				if (m_paused) {
					m_paused = false;
				} else {
					m_paused = true;
				}
			}
			break;
	}
}

void GameScene::touch(TouchState ts, uint touchid, uint x, uint y) {
	switch (m_currentState) {
		case Playing:
			if (ts == EventTouch::Pressed && m_pauseButton->inside(x, y)) {
				m_paused = !m_paused;
			} else {
				if (!m_paused) {
					m_controller->touch(ts, touchid, x, y);
					m_fcontroller->touch(ts, touchid, x, y);
				}
			}
			break;
	}
}

bool GameScene::onCollision(string g1, Node* o1, string g2, Node* o2) {
	if (g1 == "nave" && g2 == "aliens") {
		o1->m_visible = false;
		o2->m_visible = false;
		m_expSprite->setPosition(o1->m_x, o1->m_y);
		m_expSprite->resetAnimation();
		m_expSprite->m_updating =  m_expSprite->m_visible = true;
		o1->m_updating = false;
		m_currentState = Dying;
		SoundPlayer::play("death.wav");
		return true;
	}
	if (g1 == "nave" && g2 == "alien_bullets") {
		o1->m_visible = false;
		o2->m_visible = false;
		m_expSprite->setPosition(o1->m_x, o1->m_y);
		m_expSprite->resetAnimation();
		m_expSprite->m_updating = m_expSprite->m_visible = true;
		o1->m_updating = false;
		m_currentState = Dying;
		SoundPlayer::play("death.wav");
		return true;
	}
	if (g1 == "bullets" && g2 == "aliens") {
		printf("Le disparaste a un alien");
		m_spawner->destroyAlien(o2);
		m_fcontroller->clearBullet(o1);
		m_score += 100;
		//SoundPlayer::play("alien_death.wav");
		return true;
	}
	return false;
}