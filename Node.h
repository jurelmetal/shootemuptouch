#ifndef C_NODE
#define C_NODE

#include <vector>
#include <string>
using namespace std;

#include "EventRender.h"
#include "EventUpdate.h"
#include "common_structs.h"
#include "Animation.h"

class Node : public EventRender,public EventUpdate
{
public:
	// Methods
	Node();
	virtual ~Node();

	void translate(float dx, float dy);
	void setColor(float r, float g, float b);
	void setColor(float r, float g, float b, float a);
	void setAlpha(float a);
	void setPosition(float x, float y);
	void setParent(Node *parent);
	void addChild(Node *child);
	void setRotation(float rads);

	virtual void render() {}
	virtual void update(float dt);

	bool inside(uint x, uint y);

public:
	// Properties
	float m_x, m_y, m_w, m_h;
	Color m_color;
	Node* m_parent;
	float m_rotation;
	string m_name;
	bool m_updating;

protected:
	// Internal vars
	vector<Node *> m_children;
};

#endif