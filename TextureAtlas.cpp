#include "TextureAtlas.h"


TextureAtlas::TextureAtlas(const char *fileName, uint frameWidth, uint frameHeight, uint frameCount) : 
m_frameWidth(frameWidth), m_frameHeight(frameHeight), m_frameCount(frameCount)
{
	strcpy(m_fileName, fileName);
	m_texture = Iw2DCreateImage(m_fileName);
}


TextureAtlas::~TextureAtlas()
{
	delete m_texture;
}

