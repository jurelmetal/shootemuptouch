#include "Sprite.h"
#include <algorithm>

Sprite::Sprite(float posx, float posy, const char *fileName)
{
	m_x = posx;
	m_y = posy;

	m_anim = NULL;
	m_image = Iw2DCreateImage(fileName);
	m_w = m_image->GetWidth();
	m_h = m_image->GetHeight();
}

Sprite::Sprite(float posx, float posy, Animation* anim)
{
	m_x = posx;
	m_y = posy;
	m_anim = anim->clone();
	m_image = NULL;
	m_w = anim->getWidth();
	m_h = anim->getHeight();
}

Sprite::~Sprite()
{
	if (m_image != NULL) {
		delete m_image;
	}

	if (m_anim != NULL) {
		delete m_anim;
	}
}

void Sprite::update(float dt) {
	Node::update(dt);
	if (m_anim != NULL) {
		m_anim->update(dt);
	}
}

bool Sprite::animationFinished() {
	return m_anim != NULL && m_anim->hasFinished();
}

void Sprite::resetAnimation() {
	if (m_anim != NULL) {
		m_anim->reset();
	}
}

void Sprite::render()
{
	Iw2DSetColour(m_color.dword);
	CIwFMat2D currMat = Iw2DGetTransformMatrix();

	CIwFMat2D thisMat = CIwFMat2D::g_Identity;
	CIwFMat2D childrenMat = CIwFMat2D::g_Identity;
	CIwFVec2 centre;

	centre.x = m_x + m_w / 2.0f;
	centre.y = m_y + m_h / 2.0f;
	thisMat.SetRot(m_rotation, centre);
	childrenMat.SetTrans(CIwFVec2(m_x * getScaleX(), m_y * getScaleY()));

	vector<Node *> toRender;
	toRender.assign(m_children.begin(), m_children.end());
	toRender.push_back(this);

	sort(toRender.begin(), toRender.end(), EventRender::sortByZOrder);

	for (uint i = 0; i < toRender.size(); i++) {
		if (toRender[i] == this) {
			//Iw2DSetTransformMatrix(currMat * thisMat);
			Iw2DSetTransformMatrix(thisMat * currMat);
			if (m_image == NULL) {
				m_anim->render(m_x, m_y, m_w, m_h);
			} else {
				Iw2DDrawImage(m_image, CIwFVec2(m_x * getScaleX(), m_y * getScaleY()), CIwFVec2(m_w * getScaleX(), m_h * getScaleY()));
			}
		} else {
			//Iw2DSetTransformMatrix(currMat * thisMat * childrenMat);
			Iw2DSetTransformMatrix(childrenMat * thisMat * currMat);
			toRender[i]->render();
		}
	}
	Iw2DSetTransformMatrix(currMat);
}