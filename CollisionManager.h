#ifndef C_COLLISIONMANAGER
#define C_COLLISIONMANAGER

#include <string>
#include <set>
#include <map>
using namespace std;

#include "Node.h"

class CollisionManager
{
public:
	
	static void Add(Node* elem, string group);
	static void Remove(Node* elem);

	static void CheckCollisions();

	virtual bool onCollision(string g1, Node* o1, string g2, Node* o2) = 0;
	void subscribe(string group1, string group2);

protected:

	typedef map < string, set<Node*> > colgroupMap;

	static bool colliding_boundingbox(Node* a, Node* b);
	static bool colliding_pixelperfect(Node* a, Node* b);
	static colgroupMap s_collisionGroups;

	static map<pair<string, string>, vector<CollisionManager*> > s_subscriptors;
};

#endif