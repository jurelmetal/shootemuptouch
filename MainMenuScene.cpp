#include "MainMenuScene.h"
#include "TouchController.h"
#include "FiringController.h"
#include "IwGx.h"
bool MainMenuScene::setUp() {
	
	add(StarsBackground::getInstance());
	float halfw = get_surf_width() / 2.0f;
	float halfh = get_surf_height() / 2.0f;
	const char *file = "logo.png";
	CIwImage img;
	img.LoadFromFile(file);
	float imgw = img.GetWidth();
	float imgh = img.GetHeight();
	m_logo = (Sprite *) add(new Sprite(halfw - imgw/2, halfh - imgh/2, file));
	m_setUp = true;
	m_timer = 0;
	m_state = Idle;
	m_startTextVisible = false;
	return true;
}

bool MainMenuScene::tearDown() {
	delete m_logo;
	m_setUp = false;
	return true;
}

void MainMenuScene::key(KeyState ks, s3eKey key) {
	if (ks == EventKey::Pressed && key == s3eKeySpace) {
		setNextScene("GameScene");
	}
}

void MainMenuScene::touch(TouchState ts, uint touchid, uint x, uint y) {
	if (ts == EventTouch::Pressed) {
		setNextScene("GameScene");
	}
}


void MainMenuScene::update(float dt) {
	defaultUpdate(dt);
	m_timer += dt;
	if (m_timer >= 0.36f) {
		m_timer = 0;
		m_startTextVisible = !m_startTextVisible;
	}
}

void MainMenuScene::render() {
	defaultRender();
	if (m_startTextVisible) {
		IwGxLightingOn();

		IwGxFontSetCol(0xffffffff);

		//Set the formatting rect - this controls where the text appears and what it is formatted against
		IwGxFontSetRect(CIwRect(0, Iw2DGetSurfaceHeight() / 2.0f, Iw2DGetSurfaceWidth(), Iw2DGetSurfaceHeight() / 2.0f));

		//Set the alignment within the formatting rect
		IwGxFontSetAlignmentVer(IW_GX_FONT_ALIGN_MIDDLE);
		IwGxFontSetAlignmentHor(IW_GX_FONT_ALIGN_CENTRE);

		// Draw title text
		IwGxFontSetFont(startFont);
		IwGxFontDrawText("TAP TO BEGIN");

		IwGxFlush();
	}
}