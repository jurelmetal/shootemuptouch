#include "CollisionManager.h"
#include <algorithm>

map<string, set<Node*> > CollisionManager::s_collisionGroups;
map<pair<string, string>, vector<CollisionManager*> > CollisionManager::s_subscriptors;

void CollisionManager::subscribe(string group1, string group2) {
	s_subscriptors[make_pair(group1, group2)].push_back(this);
}


void CollisionManager::Add(Node* elem, string group)
{
	if (s_collisionGroups.find(group) == s_collisionGroups.end()) {
		s_collisionGroups[group] = set<Node*>();
	}
	s_collisionGroups[group].insert(elem);
}

void CollisionManager::Remove(Node* elem)
{
	for (map<string, set<Node*> >::iterator it = s_collisionGroups.begin();
		it != s_collisionGroups.end(); it++) {
		if (it->second.find(elem) != it->second.end()) {
			it->second.erase(elem);
		}
	}
}

bool CollisionManager::colliding_boundingbox(Node* a, Node* b)
{
	/*if (rect1.x < rect2.x + rect2.width &&
   rect1.x + rect1.width > rect2.x &&
   rect1.y < rect2.y + rect2.height &&
   rect1.height + rect1.y > rect2.y) {
    // collision detected!
	}*/
	if (a->m_x < b->m_x + b->m_w &&
		a->m_x + a->m_w > b->m_x &&
		a->m_y < b->m_y + b->m_h &&
		a->m_y + a->m_h > b->m_y) {
		return true;
	}
	else {
		return false;
	}
}

bool CollisionManager::colliding_pixelperfect(Node* a, Node* b) {
	if (colliding_boundingbox(a, b)) {
		return true;
	} else {
		return false;
	}
}


void CollisionManager::CheckCollisions()
{
	// Only check between different groups...

	for (colgroupMap::iterator it = s_collisionGroups.begin(); it != s_collisionGroups.end(); it++) {
		for (colgroupMap::iterator it2 = s_collisionGroups.begin(); it2 != s_collisionGroups.end(); it2++) {
			for (set<Node*>::iterator eg1 = it->second.begin(); eg1 != it->second.end(); eg1++) {
				for (set<Node*>::iterator eg2 = it2->second.begin(); eg2 != it2->second.end(); eg2++) {
					if (colliding_boundingbox(*eg1, *eg2)) {
						vector<CollisionManager*>& subscriptors = s_subscriptors[make_pair(it->first, it2->first)];
						for (uint i = 0; i < subscriptors.size(); i++) {
							if (subscriptors[i]->onCollision(it->first, *eg1, it2->first, *eg2)) {
								return;
							}
						}
					}
				}
			}
		}
	}
}