#ifndef C_ALIENSPAWNER
#define C_ALIENSPAWNER

#include <vector>
using namespace std;

#include "Node.h"
#include "Sprite.h"

class AlienSpawner;

class Alien : public Sprite {
public:
	Alien(float x, float y, Animation* anim, float ampl, float freq, float speed);
	virtual ~Alien();

	virtual void update(float dt);
	bool shouldShoot();

protected:
	CIwFVec2 m_startPos;
	float m_speed;
	float m_t;
	float m_freq;
	float m_amp;
	float m_shootProb;
	float m_shootFreq;
	bool  m_shootFlag;
	float m_shootCounter;
	friend class AlienSpawner;
};

class AlienSpawner : 
	public Node {
public:
	AlienSpawner(Animation *alienAnim, Animation *bulletAnim, float spawnRate);
	virtual ~AlienSpawner();

	virtual void update(float dt);
	virtual void render();
	void spawn();
	void shootBullet(Alien* shooter);

	void destroyAlien(Node* alien);

	void clear();

	bool m_spawning;

protected:
	vector<Alien *> m_aliens;
	vector<Sprite *> m_bullets;
	Animation* m_alienAnim;
	Animation* m_bulletAnim;

	float m_bulletSpeed;

	float m_time, m_spawnRate;
};

#endif