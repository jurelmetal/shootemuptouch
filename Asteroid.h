#ifndef C_ASTEROID
#define C_ASTEROID

#include <vector>
using namespace std;

#include "Sprite.h"
#include "Animation.h"

class Asteroid :
	public Sprite
{
public:

	enum AsteroidType {
		Normal,
		Big
	};

	Asteroid(float x, float y, AsteroidType type = Normal);
	~Asteroid();

	virtual void update(float dt);
	virtual void onCollision(Node* collider);

	static vector<Animation*> s_animations;
protected:

	CIwFVec2 m_speed;
	float m_rotSpeed;
};

#endif