#include "SoundPlayer.h"

#include "s3e.h"

map<string, SoundPlayer::sound_t> SoundPlayer::s_sounds;
SoundPlayer::SoundPlayer() {}
SoundPlayer::~SoundPlayer() {}

void SoundPlayer::play(string name) {
	if (s_sounds.find(name) != s_sounds.end()) {
		int ch = s3eSoundGetFreeChannel();
		s3eSoundChannelSetInt(ch, S3E_CHANNEL_VOLUME, S3E_SOUND_VOLUME_DEFAULT);
		s3eSoundChannelPlay(ch, s_sounds[name].data, s_sounds[name].length, 1,0);
	}
} 

void SoundPlayer::load(string name) {
	if (s_sounds.find(name) == s_sounds.end()) {
		sound_t tmp;
		s3eFile *fileHandle = s3eFileOpen(name.c_str(), "rb");
		int size = s3eFileGetSize(fileHandle);
		int16* buffer = (int16*) s3eMallocBase(size);
		memset(buffer, 0, size);
		s3eFileRead(buffer, size, 1, fileHandle);
		s3eFileClose(fileHandle);
		tmp.data = buffer;
		tmp.length = size;
		s_sounds[name] = tmp;
		int ch = s3eSoundGetFreeChannel();
		s3eSoundChannelSetInt(ch, S3E_CHANNEL_VOLUME, 0);
		s3eSoundChannelPlay(ch, buffer, size * 2, 1, 0);
	}
}