#ifndef C_TEXTUREATLAS
#define C_TEXTUREATLAS

#include "Iw2D.h"

class TextureAtlas
{
public:
	TextureAtlas(const char *fileName, uint frameWidth, uint frameHeight, uint frameCount);
	~TextureAtlas();


protected:
	char m_fileName[50];
	uint m_frameWidth, m_frameHeight, m_frameCount;
	CIw2DImage* m_texture;

	friend class Animation;
};

#endif