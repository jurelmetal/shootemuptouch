#ifndef C_SCENE
#define C_SCENE

#include <vector>
#include <map>
using namespace std;

#include "EventKey.h"
#include "EventUpdate.h"
#include "EventRender.h"
#include "EventTouch.h"

#include "Node.h"



class Scene : 
	public EventRender, public EventKey, public EventUpdate, public EventTouch {
public:
	Scene();
	virtual ~Scene();
	virtual void update(float dt);
	virtual void render();

	virtual void touch(TouchState ts, uint touchId, uint x, uint y) {}
	virtual void key(KeyState ks, s3eKey key) {}

	virtual bool setUp() { return true; }
	virtual bool tearDown() { return true; }

	Node* add(Node *elem); // unmanaged lifecycle
	Node* add(string name, Node* elem); // managed lifecycle

	void remove(Node *elem);
	void remove(string name);

	Node *get(string name);
	Node *operator[] (string name);

protected:

	void defaultUpdate(float dt);
	void defaultRender();

	class removeIfEqual {
	public:
		removeIfEqual(Node *eq) : m_eq(eq) {}
		bool operator() (Node *elem) {
			if (elem == m_eq) {
				return true;
			} else {
				return false;
			}
		}
		Node* m_eq;
	};

	bool m_setUp;
	vector<Node *> m_elements;
	map<string, Node *> m_elementsMap;
	CIwFMat2D m_transform;
};

#endif