#ifndef C_SCORESPRITE
#define C_SCORESPRITE

#include "IwGxFont.h"


#include "Node.h"
class ScoreSprite :
	public Node {
public:
	ScoreSprite(int* bindedVariable, CIwGxFont* font);
	virtual ~ScoreSprite();

	void rebind(int* var);

	virtual void update(float dt);
	virtual void render();

protected:
	int* m_bindedScore;
	CIwGxFont* m_font;
	char m_buffer[100];
};

#endif