#ifndef C_HITMAP
#define C_HITMAP

#include <vector>
#include <algorithm>

using namespace std;

#include "TextureAtlas.h"

class Hitmap {
public:
	Hitmap(const char *img);
	Hitmap(const char *atlas, uint u, uint v, uint w, uint h);

	uint m_width, m_height;
	vector<bool> m_values;
};

#endif