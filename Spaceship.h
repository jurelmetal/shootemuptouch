#ifndef C_SPACESHIP
#define C_SPACESHIP

#include "Sprite.h"

class Spaceship :
	public Sprite {
public:
	Spaceship(float posx, float posy, Animation* anim);
	virtual ~Spaceship();

	virtual void onCollision(string myGroup, string colliderGroup, Node* collider);
};

#endif