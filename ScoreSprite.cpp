#include "ScoreSprite.h"
#include "common_structs.h"
#include "IwGx.h"

ScoreSprite::ScoreSprite(int* bindedVariable, CIwGxFont* font) : 
m_bindedScore(bindedVariable)
{
	m_font = font;
}


ScoreSprite::~ScoreSprite() {}

void ScoreSprite::rebind(int* var) {
	m_bindedScore = var;
}

void ScoreSprite::update(float dt) {
	sprintf(m_buffer, "Score: %d", *m_bindedScore);
}

void ScoreSprite::render() {
	//Lighting is required to colour fonts
	IwGxLightingOn();

	//Set font colour to black
	IwGxFontSetCol(0xffa0a0a0);

	//Set the formatting rect - this controls where the text appears and what it is formatted against
	IwGxFontSetRect(CIwRect(0, 0, get_surf_width() * getScaleX(), 72 * getScaleY()));

	//Set the alignment within the formatting rect
	IwGxFontSetAlignmentVer(IW_GX_FONT_ALIGN_TOP);
	IwGxFontSetAlignmentHor(IW_GX_FONT_ALIGN_LEFT);

	// Draw title text
	IwGxFontSetFont(m_font);
	IwGxFontDrawText(m_buffer);

	IwGxFlush();
}