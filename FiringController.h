#ifndef C_FIRINGCONTROLLER
#define C_FIRINGCONTROLLER

#include <vector>
using namespace std;

#include "Sprite.h"
#include "EventTouch.h"
#include "EventKey.h"
#include "Animation.h"
#include "CollisionManager.h"


class FiringController :
	public Node, EventTouch, EventKey
{
public:
	FiringController(Animation* bullet);
	virtual ~FiringController();

	void bind(Node* target);
	void unbind();

	virtual void touch(TouchState state, uint id, uint x, uint y);
	virtual void update(float dt);
	virtual void key(KeyState ks, s3eKey key);
	virtual void render();

	void clearBullets();
	void clearBullet(Node* bullet);

	static uint s_leftLimit;
	static uint s_rightLimit;
	static uint s_maxBullets;
	static float s_bulletSpeed;

protected:

	void shoot();
	Node* m_target;
	Animation* m_bulletAnim;
	vector<Sprite *> m_bullets;

};

#endif