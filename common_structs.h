#ifndef H_COMMON_STRUCTS
#define H_COMMON_STRUCTS
#include "s3e.h"
#include "IwGxFont.h"
#include <string>
using namespace std;
union Color {
	uint32 dword;
	struct {
		uint8 a;
		uint8 r;
		uint8 g;
		uint8 b;
	};
};

template <typename T>
void attach(T* obj) {
	T::s_subscriptors.insert(obj);
}
template <typename T>
void detach(T* obj) {
	if (T::s_subscriptors.find(obj) != T::s_subscriptors.end()) {
		T::s_subscriptors.erase(T::s_subscriptors.find(obj));
	}
}

extern void setNextScene(string ns);
extern int get_surf_width();
extern int get_surf_height();
extern int targetWidth;
extern int targetHeight;
extern float getScaleX();
extern float getScaleY();
extern CIwGxFont* scoreFont;
extern CIwGxFont* startFont;

#endif