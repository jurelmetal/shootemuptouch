#include "Asteroid.h"
#include "CollisionManager.h"

vector<Animation*> Asteroid::s_animations;

Asteroid::Asteroid(float x, float y, AsteroidType type) :
Sprite(x, y, s_animations[type])
{
	float speedModulo = 100.0f;
	float speedAngle = ((float)IwRandRange(360)) * 3.14159f / 180.0f;
	float rotSpeed = 1.0f; // 1 rad/s

	m_speed.x = speedModulo * cos(speedAngle);
	m_speed.y = speedModulo * sin(speedAngle);
	m_rotSpeed = rotSpeed;
}


Asteroid::~Asteroid()
{
}

void Asteroid::update(float dt)
{
	m_x += m_speed.x * dt;
	m_y += m_speed.y * dt;
	if (m_x > Iw2DGetSurfaceWidth()) {
		m_x = 0;
	}
	if (m_y > Iw2DGetSurfaceHeight()) {
		m_y = 0;
	}
	if (m_x + m_w < 0) {
		m_x = Iw2DGetSurfaceWidth();
	}
	if (m_y + m_h < 0) {
		m_y = Iw2DGetSurfaceHeight();
	}
	m_rotation += m_rotSpeed * dt;
}

void Asteroid::onCollision(Node* collider)
{
	m_visible = false;
	collider->m_visible = false;
	CollisionManager::Remove(this);
	CollisionManager::Remove(collider);
}
