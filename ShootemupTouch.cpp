#include "s3e.h"
#include "IwDebug.h"
#include "Sprite.h"

#include "EventKey.h"

#include "TouchController.h"
#include "FiringController.h"
#include "CollisionManager.h"
#include "StarsBackground.h"

#include "GameScene.h"
#include "MainMenuScene.h"

#include "ResourceManager.h"
#include "SoundPlayer.h"
#include <iostream>

#define PRINTSZ(class)	(cout << "Class: " << #class << ": " << sizeof(class) << endl)

static string nextScene = "MainMenuScene";
static bool changedScene = false;
static Scene* currentScene = NULL;

map<string, void*> ResourceManager::s_resources;

void* transitionScene(void *) {
	if (changedScene) {
		currentScene->tearDown();
		detach<EventKey>(currentScene);
		detach<EventTouch>(currentScene);
		detach<EventUpdate>(currentScene);
		detach<EventRender>(currentScene);
		delete currentScene;
		if (nextScene == "MainMenuScene") {
			currentScene = new MainMenuScene();
		} else if (nextScene == "GameScene") {
			currentScene = new GameScene();
		}
		currentScene->setUp();
		attach<EventKey>(currentScene);
		attach<EventTouch>(currentScene);
		attach<EventUpdate>(currentScene);
		attach<EventRender>(currentScene);
		changedScene = false;
	}
	return NULL;
}

void setNextScene(string ns) {
	if (!changedScene) {
		changedScene = nextScene != ns;
		nextScene = ns;
	}
}

void initResources() {
	TextureAtlas *atlasMotor = new TextureAtlas("motor.png", 102, 102, 25);
	TextureAtlas * atlasBullet = new TextureAtlas("bullet.png", 41, 12, 1);
	Animation* animMotor = new Animation(atlasMotor, 5, 15, 1.0f / 24.0f);
	animMotor->setRandomized(true);
	Animation *bulletAnim = new Animation(atlasBullet, 0, 1, 1.0f / 24.0f);

	Sprite* motor = new Sprite(0, 0, animMotor);
	Sprite* nave = new Sprite(0, 0, "blueship2.png");
	nave->addChild(motor);
	motor->setPosition(-40 * getScaleX(), -6 * getScaleY());
	motor->m_zOrder = -1;
	//motor->setRotation(-3.14159f / 2.0f);

	TextureAtlas* alienAtlas = new TextureAtlas("alien.png", 50, 50, 1);
	Animation* alienAnim = new Animation(alienAtlas, 0, 1, 1.0f / 24.0f);

	TextureAtlas* explosionAtlas = new TextureAtlas("explosion.png", 67*2, 67*2, 32);
	Animation* explosionAnim = new Animation(explosionAtlas, 0, 32, 1.0f / 24.0f);
	explosionAnim->setLooping(false);

	Sprite* explosionSprite = new Sprite(0, 0, explosionAnim);
	int dummy = 0;
	ScoreSprite* scoreSprite = new ScoreSprite(&dummy, scoreFont);
	Sprite *pausebutton = new Sprite(0, 0, "pause.png");

	ResourceManager::addResource<TextureAtlas>(atlasMotor, "atlas_motor");
	ResourceManager::addResource<TextureAtlas>(atlasBullet, "atlas_bullet");
	ResourceManager::addResource<TextureAtlas>(alienAtlas, "atlas_alien");
	ResourceManager::addResource<TextureAtlas>(explosionAtlas, "atlas_explosion");

	ResourceManager::addResource<Animation>(animMotor, "anim_motor");
	ResourceManager::addResource<Animation>(bulletAnim, "anim_bullet");
	ResourceManager::addResource<Animation>(alienAnim, "anim_alien");
	ResourceManager::addResource<Animation>(explosionAnim, "anim_explosion");

	ResourceManager::addResource<Sprite>(motor, "sprite_motor");
	ResourceManager::addResource<Sprite>(nave, "sprite_nave");
	ResourceManager::addResource<Sprite>(explosionSprite, "sprite_explosion");
	ResourceManager::addResource<ScoreSprite>(scoreSprite, "sprite_score");
	ResourceManager::addResource<Sprite>(pausebutton, "sprite_pause");

	SoundPlayer::load("alien_death.wav");
	SoundPlayer::load("alien_shoot.wav");
	SoundPlayer::load("shoot.wav");
	SoundPlayer::load("death.wav");
}

void terminateResources() {
	ResourceManager::deleteResource<TextureAtlas>("atlas_motor");
	ResourceManager::deleteResource<TextureAtlas>("atlas_bullet");
	ResourceManager::deleteResource<TextureAtlas>("atlas_alien");
	ResourceManager::deleteResource<TextureAtlas>("atlas_explosion");

	ResourceManager::deleteResource<Animation>("anim_alien");
	ResourceManager::deleteResource<Animation>("anim_explosion");
	ResourceManager::deleteResource<Animation>("anim_motor");
	ResourceManager::deleteResource<Animation>("anim_bullet");

	ResourceManager::deleteResource<Sprite>("sprite_motor");
	ResourceManager::deleteResource<Sprite>("sprite_nave");
	ResourceManager::deleteResource<Sprite>("sprite_explosion");
	ResourceManager::deleteResource<ScoreSprite>("sprite_score");
	ResourceManager::deleteResource<Sprite>("sprite_pause");
}

/*
int get_surf_width() {
	int32 osid = s3eDeviceGetInt(S3E_DEVICE_OS);
	if (osid == S3E_OS_ID_WP8 || osid == S3E_OS_ID_WP81) {
		return Iw2DGetSurfaceHeight();
	} else {
		return Iw2DGetSurfaceWidth();
	}
}

int get_surf_height() {
	int32 osid = s3eDeviceGetInt(S3E_DEVICE_OS);
	if (osid == S3E_OS_ID_WP8 || osid == S3E_OS_ID_WP81) {
		return Iw2DGetSurfaceWidth();
	} else {
		return Iw2DGetSurfaceHeight();
	}
}
*/

int targetWidth = 1280;
int targetHeight = 720;
CIwGxFont* scoreFont = NULL;
CIwGxFont* startFont = NULL;

float getScaleX() {
	return (float) Iw2DGetSurfaceWidth() / (float) targetWidth;
}

float getScaleY() {
	return (float) Iw2DGetSurfaceHeight() / (float) targetHeight;
}

int get_surf_width() { return targetWidth; }
int get_surf_height() { return targetHeight; }

// Main entry point for the application
int main()
{
    //Initialise graphics system(s)
	Iw2DInit();
	IwGxFontInit();
	srand(time(NULL));
	IwRandSeed(time(NULL));

	s3eSoundSetInt(S3E_SOUND_DEFAULT_FREQ, 44100);

	TouchController::s_rightLimit = get_surf_width() / 2.0f;
	TouchController::s_keySpeed = 500.0f / 60.0f;
	FiringController::s_bulletSpeed = 800.0f;
	FiringController::s_leftLimit = get_surf_width() / 2.0f;
	FiringController::s_rightLimit = get_surf_width();

	scoreFont = IwGxFontCreateTTFont("Souses.ttf", 12 * getScaleY());
	startFont = IwGxFontCreateTTFont("Souses.ttf", 8 * getScaleY());

	Iw2DSetTransformMatrix(CIwFMat2D::g_Identity);

	s3eKey initKeys[] = {s3eKeyEsc, s3eKeyLeft, s3eKeyRight, s3eKeyUp, s3eKeyDown, s3eKeySpace};
	EventKey::s_subscribedKeys.assign(initKeys, initKeys + sizeof(initKeys)/sizeof(s3eKey));
	
	uint64 lastFrameMs = s3eTimerGetMs();
	uint64 currentFrameMs = lastFrameMs;
	uint64 delta;
	float secondsDelta;

	StarsBackground::getInstance();

	initResources();

	currentScene = new MainMenuScene();

	if (!currentScene->setUp()) {
		return -1;
	}

	attach<EventUpdate>(currentScene);
	attach<EventRender>(currentScene);
	attach<EventKey>(currentScene);
	attach<EventTouch>(currentScene);

	

    // Loop forever, until the user or the OS performs some action to quit the app
    while (!s3eDeviceCheckQuitRequest())
    {
        //Update the input systems
        s3eKeyboardUpdate();
        s3ePointerUpdate();

		currentFrameMs = s3eTimerGetMs();
		delta = currentFrameMs - lastFrameMs;
		lastFrameMs = currentFrameMs;

		secondsDelta = (float)delta / 1000.0f;

		secondsDelta = min(secondsDelta, 1.0f / 24.0f);
        
		EventUpdate::emit(secondsDelta);
		EventTouch::checkTouchEvents();
		EventKey::checkKeyEvents();

        // Your rendering/app code goes here.
		
		Iw2DSurfaceClear(0xff000000);
		Iw2DSetColour(0xffffffff);

		EventRender::emit();

		Iw2DSurfaceShow();

		if (changedScene) {
			transitionScene(NULL);
		}

        // Sleep for 0ms to allow the OS to process events etc.
        s3eDeviceYield(0);

    }

	detach<EventUpdate>(currentScene);
	detach<EventRender>(currentScene);
	detach<EventKey>(currentScene);
	detach<EventTouch>(currentScene);

	currentScene->tearDown();

	delete currentScene;

	terminateResources();

	delete StarsBackground::getInstance();

	IwGxFontDestroyTTFont(scoreFont);
	IwGxFontDestroyTTFont(startFont);

    //Terminate modules being used
	IwGxFontTerminate();
	Iw2DTerminate();

    // Return
    return 0;
}
