#pragma once
#include "D:\games\ShootemupTouch\Scene.h"

#include "StarsBackground.h"
#include "Sprite.h"

class MainMenuScene :
	public Scene {
public:
	virtual bool setUp();
	virtual bool tearDown();

	virtual void key(KeyState ks, s3eKey key);
	virtual void touch(TouchState ts, uint touchid, uint x, uint y);
	virtual void update(float dt);
	virtual void render();

	enum MenuState {
		Idle,
		Fading
	};

protected:
	StarsBackground* m_background;
	MenuState m_state;
	Sprite* m_logo;
	float m_timer;
	bool m_startTextVisible;

};

