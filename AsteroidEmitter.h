#pragma once
#include "ObjectEmitter.h"
#include "Asteroid.h"
#include "CollisionManager.h"

class AsteroidEmitter :
	public ObjectEmitter
{
public:
	AsteroidEmitter(float rate);

	virtual void emit(float x, float y);
};

