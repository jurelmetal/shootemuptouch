#ifndef C_GAMESCENE
#define C_GAMESCENE

#include "Scene.h"
#include "TouchController.h"
#include "FiringController.h"
#include "Sprite.h"
#include "StarsBackground.h"
#include "AlienSpawner.h"
#include "ScoreSprite.h"

class GameScene :
	public Scene, CollisionManager {
public:

	virtual bool setUp();
	virtual bool tearDown();

	void reset();

	virtual void update(float dt);
	virtual void render();

	virtual void key(KeyState ks, s3eKey key);
	virtual void touch(TouchState ts, uint touchid, uint x, uint y);

	virtual bool onCollision(string g1, Node* o1, string g2, Node* o2);

	enum GameStates {
		Intro,
		Playing,
		Dying
	};

protected:

	TouchController* m_controller;
	FiringController* m_fcontroller;

	GameStates m_currentState;

	Sprite* m_motor;
	Sprite* m_nave;

	StarsBackground* m_background;

	Animation* m_animMotor;
	Animation* m_bulletAnim;
	TextureAtlas* m_atlasMotor;
	TextureAtlas* m_atlasBullet;

	AlienSpawner* m_spawner;

	Animation* m_alienAnim;
	TextureAtlas* m_alienAtlas;
	Sprite *m_expSprite;

	int m_score;
	ScoreSprite* m_scoreSprite;
	Sprite* m_pauseButton;

	bool m_paused;

};

#endif