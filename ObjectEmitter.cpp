#include "ObjectEmitter.h"
#include <algorithm>
#include "Iw2D.h"
#include "CollisionManager.h"


ObjectEmitter::ObjectEmitter(float emitRate)
{
	m_emitRate = emitRate;
	m_counter = 0;
}


ObjectEmitter::~ObjectEmitter()
{
	for (uint i = 0; i < m_instances.size(); i++) {
		delete m_instances[i];
	}
}


void ObjectEmitter::update(float dt)
{
	m_counter += dt;
	if (m_counter >= 1.0f / m_emitRate) {
		m_counter = 0;
		float x = IwRandRange(get_surf_width());
		float y = IwRandRange(get_surf_height());
		emit(x, y);
	}
}