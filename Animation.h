#ifndef C_ANIMATION
#define C_ANIMATION
#include <vector>
using namespace std;

#include "TextureAtlas.h"

class Animation
{
public:
	Animation(TextureAtlas* atlas, uint start, uint count, float frameTime);
	~Animation();

	CIw2DImage *getAtlas();

	void render(float x, float y, float w, float h);
	void update(float dt);
	void setLooping(bool loops);
	bool hasFinished();
	void reset();
	uint getWidth();
	uint getHeight();
	void setRandomized(bool randomized);

	Animation *clone();

protected:
	TextureAtlas* m_atlas;
	uint m_start, m_count;
	float m_frameTime;
	uint m_currentFrame;
	float m_currentFrameTime;
	bool m_looping;
	bool m_randomized;

};

#endif