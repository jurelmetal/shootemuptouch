#include "Animation.h"
#include "common_structs.h"

Animation::Animation(TextureAtlas* atlas, uint start, uint count, float frameTime) : 
m_atlas(atlas), m_start(start), m_count(count), m_frameTime(frameTime)
{
	m_currentFrame = start;
	m_looping = true;
	m_randomized = false;
}


Animation::~Animation()
{

}

Animation* Animation::clone()
{
	Animation *copy = new Animation(m_atlas, m_start, m_count, m_frameTime);
	copy->setRandomized(m_randomized);
	copy->setLooping(m_looping);
	return copy;
}

uint Animation::getWidth()
{
	return m_atlas->m_frameWidth;
}

uint Animation::getHeight()
{
	return m_atlas->m_frameHeight;
}

CIw2DImage* Animation::getAtlas()
{
	return m_atlas->m_texture;
}

void Animation::setLooping(bool loops)
{
	m_looping = loops;
}

bool Animation::hasFinished() {
	if (m_looping) {
		return false;
	} else {
		return m_currentFrame == m_start + m_count - 1;
	}
}

void Animation::setRandomized(bool randomized)
{
	m_randomized = randomized;
}

void Animation::reset() {
	m_currentFrame = 0;
	m_currentFrameTime = 0.0f;
}

void Animation::update(float dt)
{
	m_currentFrameTime += dt;
	if (m_currentFrameTime >= m_frameTime) {
		m_currentFrameTime = 0;
		if (m_randomized) {
			m_currentFrame = IwRandMinMax(m_start, m_start + m_count - 1);
		}
		else {
			m_currentFrame++;
		}
		if (m_currentFrame >= m_start + m_count) {
			if (m_looping) {
				m_currentFrame = m_start;
			}
			else {
				m_currentFrame = m_start + m_count - 1;
			}
		}
	}
}

void Animation::render(float x, float y, float w, float h)
{
	CIw2DImage *img = getAtlas();
	uint img_width = img->GetWidth();
	uint img_height = img->GetHeight();
	uint frame_width = m_atlas->m_frameWidth;
	uint frame_height = m_atlas->m_frameHeight;

	uint cols = img_width / frame_width;
	uint rows = img_height / frame_height;

	uint current_row = m_currentFrame / cols;
	uint current_col = m_currentFrame - current_row * cols;

	uint originX = current_col * frame_width;
	uint originY = current_row * frame_height;

	Iw2DDrawImageRegion(img, CIwFVec2(x * getScaleX(), y * getScaleY()), CIwFVec2(w * getScaleX(), h * getScaleY()), CIwFVec2(originX, originY), CIwFVec2(frame_width, frame_height));
}
