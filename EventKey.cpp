#include "EventKey.h"
#include <iostream>
using namespace std;

set<EventKey *> EventKey::s_subscriptors;
vector<s3eKey> EventKey::s_subscribedKeys;

EventKey::EventKey()
{

}


EventKey::~EventKey() {
	
}

void EventKey::emit(KeyState state, s3eKey key) {
	for (set < EventKey * >::iterator it = s_subscriptors.begin();
		it != s_subscriptors.end();
		it++) {
		(*it)->key(state, key);
	}
}

void EventKey::checkKeyEvents() {
	for (uint i = 0; i < s_subscribedKeys.size(); i++) {
		s3eKey it = s_subscribedKeys[i];
		int32 state = s3eKeyboardGetState(it);
		if (state & S3E_KEY_STATE_PRESSED) {
			emit(Pressed, it);
		}
		if (state & S3E_KEY_STATE_DOWN) {
			emit(Down, it);
		}
		if (state & S3E_KEY_STATE_RELEASED) {
			emit(Released, it);
		}
	}
}