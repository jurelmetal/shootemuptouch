#ifndef C_EVENTTOUCH
#define C_EVENTTOUCH

#include "s3e.h"

#include <set>
using namespace std;

class EventTouch
{
public:
	enum TouchState {
		Pressed,
		Moved,
		Released
	};
	EventTouch();
	virtual ~EventTouch();
	virtual void touch(TouchState, uint, uint, uint) = 0;
	static void emit(TouchState, uint, uint, uint);
	static void checkTouchEvents();

	static set<EventTouch *> s_subscriptors;
	static bool s_touchedBefore[S3E_POINTER_TOUCH_MAX];
};

#endif