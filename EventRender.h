#ifndef C_EVENTRENDER
#define C_EVENTRENDER

#include <set>
using namespace std;

class EventRender
{
public:
	EventRender();
	virtual ~EventRender();
	virtual void render() = 0;
	static void emit();

	bool m_visible;
	int m_zOrder;

	static bool sortByZOrder(EventRender* a, EventRender* b);

	static set<EventRender*> s_subscriptors;
};

#endif