#ifndef C_SOUNDPLAYER
#define C_SOUNDPLAYER

#include <string>
#include <map>
using namespace std;


class SoundPlayer {
public:
	SoundPlayer();
	~SoundPlayer();


	static void play(string name);
	static void load(string name);

	struct sound_t {
		int16 *data;
		int length;
	};
protected:
	static map<string, sound_t> s_sounds;
};

#endif