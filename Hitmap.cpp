#include "Hitmap.h"


Hitmap::Hitmap(const char* img) {
	CIwImage im, hitmap;
	im.LoadFromFile(img);
	m_width = im.GetWidth();
	m_height = im.GetHeight();

	hitmap.SetFormat(CIwImage::A_8);
	hitmap.SetWidth(m_width);
	hitmap.SetHeight(m_height);
	hitmap.ConvertToImage(&im);

	const uint8* texels = hitmap.GetTexels();
	
	for (uint i = 0; i < m_width*m_height; i++) {
		m_values.push_back(texels[i] > 0x55);
	}
}

Hitmap::Hitmap(const char* ta, uint u, uint v, uint w, uint h) {
	CIwImage im, hitmap;
	im.LoadFromFile(ta);
	m_width = w;
	m_height = h;

	hitmap.SetFormat(CIwImage::A_8);
	hitmap.SetWidth(im.GetWidth());
	hitmap.SetHeight(im.GetHeight());
	hitmap.ConvertToImage(&im);

	const uint8* texels = hitmap.GetTexels();

	for (uint i = h * v; i < h * (v + 1); i++) {
		for (uint j = w * u; j < w * (u + 1); j++) {
			m_values.push_back(texels[i*h + j] > 0x55);
		}
	}
}