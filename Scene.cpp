#include "Scene.h"
#include <algorithm>

Scene::Scene()
{
	m_transform = CIwFMat2D::g_Identity;
	m_setUp = false;
}


Scene::~Scene()
{
	for (map<string, Node*>::iterator elem = m_elementsMap.begin(); elem != m_elementsMap.end(); elem++) {
		delete elem->second;
	}
}

void Scene::defaultUpdate(float dt) {
	if (m_setUp) {
		for (uint i = 0; i < m_elements.size(); i++) {
			Node* elem = m_elements[i];
			if (elem->m_updating) {
				elem->update(dt);
			}
		}
	} 
}

void Scene::defaultRender() {
	Iw2DSetTransformMatrix(m_transform);
	sort(m_elements.begin(), m_elements.end(), EventRender::sortByZOrder);
	for (uint i = 0; i < m_elements.size(); i++) {
		Node* elem = m_elements[i];
		if (elem->m_visible)
			elem->render();
	}
	Iw2DSetTransformMatrix(CIwFMat2D::g_Identity);
}

void Scene::update(float dt) {
	defaultUpdate(dt);
}

void Scene::render() {
	defaultRender();
}

Node* Scene::add(Node* elem) {
	elem->m_zOrder = m_elements.size();
	m_elements.push_back(elem);
	return elem;
}

Node* Scene::add(string name, Node* elem) {
	if (m_elementsMap.find(name) == m_elementsMap.end()) {
		add(elem);
		m_elementsMap[name] = elem;
		return elem;
	}
	return NULL;
}

void Scene::remove(Node *elem) {
	remove_if(m_elements.begin(), m_elements.end(), removeIfEqual(elem));
}

void Scene::remove(string name) {
	map<string, Node*>::iterator toRemove;
	if ((toRemove = m_elementsMap.find(name)) != m_elementsMap.end()) {
		delete toRemove->second;
		remove(toRemove->second);
		m_elementsMap.erase(name);
	}
}

Node* Scene::get(string name) {
	map<string, Node*>::iterator elem;
	if ((elem = m_elementsMap.find(name)) != m_elementsMap.end()) {
		return elem->second;
	}
	return NULL;
}

Node *Scene::operator[] (string name) {
	return get(name);
}