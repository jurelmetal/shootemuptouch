#ifndef C_RESOURCEMANAGER
#define C_RESOURCEMANAGER

#include <string>
#include <map>
using namespace std;

class ResourceManager {
public:
	ResourceManager();
	~ResourceManager();

	template<typename T> static T* getResource(string name);
	template<typename T> static void addResource(T* res, string name);
	template<typename T> static void deleteResource(string name);
	
protected:
	static map<string, void*> s_resources;
};



template<typename T>
T* ResourceManager::getResource(string name) {
	if (s_resources.find(name) != s_resources.end()) {
		return reinterpret_cast<T*>(s_resources[name]);
	} else {
		return NULL;
	}
}

template<typename T>
void ResourceManager::addResource(T* res, string name) {
	if (s_resources.find(name) != s_resources.end()) {
		return;
	} else {
		s_resources[name] = res;
	}
}

template<typename T>
void ResourceManager::deleteResource(string name) {
	if (s_resources.find(name) != s_resources.end()) {
		T* res = reinterpret_cast<T *>(s_resources[name]);
		s_resources.erase(name);
		delete res;
	}
}

#endif