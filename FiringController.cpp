#include "FiringController.h"
#include <algorithm>
#include "SoundPlayer.h"

uint FiringController::s_leftLimit = 0;
uint FiringController::s_rightLimit = 0;
uint FiringController::s_maxBullets = 10;
float FiringController::s_bulletSpeed = 0.0f;

FiringController::FiringController(Animation* bullet)
{
	m_bulletAnim = bullet;
	m_target = NULL;
}


FiringController::~FiringController()
{
	for (uint i = 0; i < m_bullets.size(); i++) {
		if (m_bullets[i] != NULL) {
			delete m_bullets[i];
		}
	}
}

void FiringController::bind(Node* target)
{
	m_target = target;
}

void FiringController::unbind()
{
	m_target = NULL;
}

void FiringController::touch(TouchState state, uint id, uint x, uint y)
{
	if (s_leftLimit <= x && x <= s_rightLimit) {
		if (state == EventTouch::Pressed) {
			shoot();
		}
	}
}

bool isNull(Sprite* elem)
{
	return elem == NULL;
}

void FiringController::update(float dt)
{
	for (uint i = 0; i < m_bullets.size(); i++) {
		if (m_bullets[i] != NULL) {
			m_bullets[i]->translate(s_bulletSpeed * dt, 0);
			if (m_bullets[i]->m_x > s_rightLimit) {
				delete m_bullets[i];
				CollisionManager::Remove(m_bullets[i]);
				m_bullets[i] = NULL;
			}
		}
	}
	vector<Sprite*>::iterator new_end
		= remove_if(m_bullets.begin(), m_bullets.end(), isNull);
	m_bullets.erase(new_end, m_bullets.end());
}

void FiringController::render() {
	for (uint i = 0; i < m_bullets.size(); i++) {
		if (m_bullets[i] != NULL) {
			m_bullets[i]->render();
		}
	}
}

void FiringController::shoot()
{
	if (m_bullets.size() < s_maxBullets) {
		SoundPlayer::play("shoot.wav");
		float bulletx = m_target->m_x + m_target->m_w;
		float bullety = m_target->m_y + m_target->m_h / 2;
		Sprite* bullet = new Sprite(bulletx, bullety, m_bulletAnim);
		CollisionManager::Add(bullet, "bullets");
		m_bullets.push_back(bullet);
	}
}

void FiringController::key(KeyState ks, s3eKey key) {
	if (ks == EventKey::Pressed && key == s3eKeySpace) {
		shoot();
	}
}

void FiringController::clearBullets() {
	for (uint i = 0; i < m_bullets.size(); i++) {
		if (m_bullets[i] != NULL) {
			CollisionManager::Remove(m_bullets[i]);
			delete m_bullets[i];
		}
	}
	m_bullets.clear();
}
template<typename T>
class ifEqualRemove {
public:
	ifEqualRemove(T* elem) { m_elem = elem;  }
	bool operator() (T* elem) {
		if (elem != NULL && elem == m_elem) {
			CollisionManager::Remove(elem);
			delete elem;
			return true;
		} else {
			return false;
		}
	}

	T* m_elem;
};
void FiringController::clearBullet(Node* bullet) {
	vector < Sprite* >::iterator newend = remove_if(m_bullets.begin(), m_bullets.end(), ifEqualRemove<Node>(bullet));
	m_bullets.erase(newend, m_bullets.end());
		
}