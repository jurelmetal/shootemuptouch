#ifndef C_EVENTUPDATE
#define C_EVENTUPDATE

#include <set>
using namespace std;

class EventUpdate
{
public:
	EventUpdate();
	virtual ~EventUpdate();
	virtual void update(float) = 0;
	static void emit(float);

	static set<EventUpdate *> s_subscriptors;
};

#endif