#include "StarsBackground.h"

#include "Iw2D.h"


float StarsBackground::offscreenStarMargin = 100;

StarsBackground::StarsBackground()
{
	uint planes = 3;
	uint starsPerPlane = 30;
	uint screenW = get_surf_width();
	uint screenH = get_surf_height();
	Star tmpstar;
	for (uint i = 0; i < planes; i++) {
		for (uint j = 0; j < starsPerPlane; j++) {
			tmpstar.m_x = IwRandRange(screenW);
			tmpstar.m_y = IwRandRange(screenH);
			tmpstar.m_d = (float)IwRandRange(40) / 10.0f;
			tmpstar.m_speed = (float)IwRandMinMax(50, 300);
			tmpstar.m_color.dword = 0xffffffff;
			tmpstar.m_color.r -= (uint8) IwRandMinMax(0x00, 0x11);
			tmpstar.m_color.a -= (uint8) IwRandMinMax(0x00, 0x11);
			tmpstar.m_color.g -= (uint8) IwRandMinMax(0x00, 0x11);
			tmpstar.m_color.b -= (uint8) IwRandMinMax(0x00, 0x11);
			m_stars.push_back(tmpstar);
		}
	}
	// draw first
	m_zOrder = -1000;
}


StarsBackground::~StarsBackground()
{

}

void StarsBackground::update(float dt)
{
	float delta;
	for (uint i = 0; i < m_stars.size(); i++) {
		delta = m_stars[i].m_speed * dt;
		m_stars[i].m_x -= delta;
		if (m_stars[i].m_x < -offscreenStarMargin) {
			m_stars[i].m_x = get_surf_width() + offscreenStarMargin;
		}
	}
}

void StarsBackground::render()
{
	Color currColor;
	for (uint i = 0; i < m_stars.size(); i++) {
		currColor = m_stars[i].m_color;
		Iw2DSetColour(currColor.dword);
		Iw2DFillArc(CIwFVec2(m_stars[i].m_x * getScaleX(), m_stars[i].m_y * getScaleY()), CIwFVec2(m_stars[i].m_d * getScaleY(), m_stars[i].m_d * getScaleY()), IW_ANGLE_FROM_DEGREES(0), IW_ANGLE_FROM_DEGREES(360), 40);
	}
	Iw2DSetColour(0xffffffff);
}

StarsBackground* StarsBackground::s_instance = NULL;

StarsBackground* StarsBackground::getInstance() {
	if (s_instance == NULL) {
		s_instance = new StarsBackground();
	}
	return s_instance;
}