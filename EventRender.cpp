#include "EventRender.h"
#include <vector>
#include <algorithm>
#include <iostream>
using namespace std;

set<EventRender*> EventRender::s_subscriptors;

EventRender::EventRender()
{
	m_visible = true;
	/*
	m_zOrder = s_subscriptors.size();
	s_subscriptors.insert(this);
	*/
}


EventRender::~EventRender()
{

}

bool EventRender::sortByZOrder(EventRender* a, EventRender* b)
{
	return a->m_zOrder < b->m_zOrder;
}

void EventRender::emit()
{
	vector<EventRender*> elements(s_subscriptors.begin(), s_subscriptors.end());
	sort(elements.begin(), elements.end(), EventRender::sortByZOrder);
	for (uint i = 0; i < elements.size(); i++) {
		if (elements[i]->m_visible) {
			elements[i]->render();
		}
	}
}

