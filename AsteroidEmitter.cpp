#include "AsteroidEmitter.h"


AsteroidEmitter::AsteroidEmitter(float rate) : 
ObjectEmitter(rate)
{

}


void AsteroidEmitter::emit(float x, float y)
{
	Asteroid *a = new Asteroid(x, y);
	m_instances.push_back(a);
	CollisionManager::Add(a, "asteroids");
}