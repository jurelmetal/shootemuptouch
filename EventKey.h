#ifndef C_EVENTKEY
#define C_EVENTKEY

#include <set>
#include <vector>
using namespace std;

#include "s3e.h"

class EventKey {
public:
	enum KeyState {
		Pressed,
		Down,
		Released
	};

	EventKey();
	virtual ~EventKey();
	virtual void key(KeyState state, s3eKey key) = 0;
	static void emit(KeyState, s3eKey);
	static void checkKeyEvents();

	static set<EventKey *> s_subscriptors;
	static vector<s3eKey> s_subscribedKeys;
};

#endif