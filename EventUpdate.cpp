#include "EventUpdate.h"
#include <iostream>
using namespace std;
set<EventUpdate *> EventUpdate::s_subscriptors;

EventUpdate::EventUpdate()
{
	//s_subscriptors.insert(this);
}

EventUpdate::~EventUpdate()
{
	

}

void EventUpdate::emit(float dt) {
	for (set < EventUpdate * >::iterator it = s_subscriptors.begin();
		it != s_subscriptors.end();
		it++) {
		(*it)->update(dt);
	}
}