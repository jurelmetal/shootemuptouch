#include "TouchController.h"

uint TouchController::s_leftLimit = 0;
uint TouchController::s_rightLimit = 0;
float TouchController::s_keySpeed = 0.0f;

TouchController::TouchController() : 
m_lastTouch(0,0)
{
	m_target = NULL;
	m_pressed = false;
}


TouchController::~TouchController()
{
}

void TouchController::bind(Node* target)
{
	m_target = target;
}

void TouchController::unbind()
{
	m_target = NULL;
}

Node* TouchController::getTarget()
{
	return m_target;
}

void TouchController::touch(TouchState state, uint id, uint x, uint y)
{
	if (s_leftLimit <= x && x <= s_rightLimit) {
		if (state == EventTouch::Pressed) {
			m_pressed = true;
			m_lastTouch = CIwFVec2(x, y);
		}
		else if (state == EventTouch::Moved) {
			if (m_pressed) {
				CIwFVec2 delta = CIwFVec2(x, y) - m_lastTouch;
				if (m_target != NULL) {
					m_target->translate(delta.x, delta.y);
					confine(m_target);
				}
				m_lastTouch = CIwFVec2(x, y);
			}
			else {
				m_pressed = true;
				m_lastTouch = CIwFVec2(x, y);
			}
		}
		else { // released
			m_pressed = false;
		}
	}
}



void TouchController::key(KeyState ks, s3eKey key) {
	if (ks == EventKey::Down && m_target != NULL) {
		CIwFVec2 delta(0.0f, 0.0f);
		switch (key) {
			case s3eKeyUp:
				delta.y -= s_keySpeed;
				break;
			case s3eKeyDown:
				delta.y += s_keySpeed;
				break;
			case s3eKeyLeft:
				delta.x -= s_keySpeed;
				break;
			case s3eKeyRight:
				delta.x += s_keySpeed;
				break;
			default:
				break;
		}
		m_target->translate(delta.x, delta.y);
		confine(m_target);
	}
}

void TouchController::confine(Node *target) {
	if (target->m_x < 0) {
		target->m_x = 0;
	}
	if (target->m_y < 0) {
		target->m_y = 0;
	}
	if (get_surf_width() - target->m_w < target->m_x) {
		target->m_x = get_surf_width() - target->m_w;
	}
	if (get_surf_height() - target->m_h < target->m_y) {
		target->m_y = get_surf_height() - target->m_h;
	}
	target->setPosition(target->m_x, target->m_y); // update children
}