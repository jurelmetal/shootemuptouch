#include "EventTouch.h"
#include "common_structs.h"
#include <iostream>
using namespace std;
bool EventTouch::s_touchedBefore[S3E_POINTER_TOUCH_MAX];

set<EventTouch *> EventTouch::s_subscriptors;

EventTouch::EventTouch()
{
	//s_subscriptors.insert(this);
}

EventTouch::~EventTouch()
{

}

void EventTouch::emit(TouchState ts, uint touchId, uint x, uint y) {
	for (set < EventTouch * >::iterator it = s_subscriptors.begin();
		it != s_subscriptors.end();
		it++) {
		(*it)->touch(ts, touchId, x, y);
	}
}

void EventTouch::checkTouchEvents() {
	for (uint i = 0; i < S3E_POINTER_TOUCH_MAX; i++) {
		s3ePointerState ptrState = s3ePointerGetTouchState(i);
		int32 px = s3ePointerGetTouchX(i) / (float) getScaleX();
		int32 py = s3ePointerGetTouchY(i) / (float) getScaleY();
		if (ptrState == S3E_POINTER_STATE_DOWN) {
			TouchState state;
			if (s_touchedBefore[i]) {
				state = EventTouch::Moved;
			} else {
				state = EventTouch::Pressed;
				s_touchedBefore[i] = true;
			}
			emit(state, i, px, py);
		} else if (ptrState == S3E_POINTER_STATE_RELEASED) {
			s_touchedBefore[i] = false;
			emit(EventTouch::Released, i, px, py);
		}
	}
}