#ifndef C_SPRITE
#define C_SPRITE

#include "Node.h"
#include "EventUpdate.h"
#include "EventRender.h"

class Sprite :
	public Node
{
public:
	Sprite(float posx, float posy, const char *fileName);
	Sprite(float posx, float posy, Animation* anim);
	virtual ~Sprite();

	bool animationFinished();
	void resetAnimation();

	virtual void render();
	virtual void update(float dt);

protected:
	Animation* m_anim;
	CIw2DImage* m_image;
};

#endif