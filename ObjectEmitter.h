#ifndef C_OBJECTEMITTER
#define C_OBJECTEMITTER

#include <vector>
using namespace std;

#include "Sprite.h"

class ObjectEmitter : public EventUpdate
{
public:
	ObjectEmitter(float emitRate);
	~ObjectEmitter();

	void update(float dt);

	virtual void emit(float x, float y) = 0;

protected:
	vector<Sprite *> m_instances;
	float m_emitRate;
	float m_counter;
};

#endif