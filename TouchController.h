#ifndef C_TOUCHCONTROLLER
#define C_TOUCHCONTROLLER

#include "EventTouch.h"
#include "EventKey.h"
#include "Node.h"

class TouchController :
	public EventTouch, EventKey
{
public:
	TouchController();
	~TouchController();

	void bind(Node* target);
	void unbind();

	Node* getTarget();

	virtual void touch(TouchState state, uint id, uint x, uint y);
	virtual void key(KeyState ks, s3eKey key);

	void confine(Node *target);

	static uint s_leftLimit;
	static uint s_rightLimit;
	static float s_keySpeed;

protected:
	Node* m_target;
	bool m_pressed;
	CIwFVec2 m_lastTouch;


	
	
};

#endif