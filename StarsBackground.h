#ifndef C_STARS_BACKGROUND
#define C_STARS_BACKGROUND

#include "Node.h"
#include "common_structs.h"
#include <vector>
using namespace std;


class StarsBackground : public Node
{
public:

	struct Star 
	{
		float m_x, m_y, m_d;
		float m_speed;
		Color m_color;
	};

	StarsBackground();
	virtual ~StarsBackground();

	virtual void update(float dt);
	virtual void render();

	static float offscreenStarMargin;
	
	static StarsBackground* getInstance();



protected:
	vector<Star> m_stars;
	static StarsBackground* s_instance;
};

#endif