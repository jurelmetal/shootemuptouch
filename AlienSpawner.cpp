#include "AlienSpawner.h"
#include "CollisionManager.h"
#include <algorithm>
#include "SoundPlayer.h"


Alien::Alien(float x, float y, Animation* anim, float ampl, float freq, float speed) : 
Sprite(x, y, anim)
{
	m_amp = ampl;
	m_freq = freq;
	m_t = 0;
	m_speed = speed;
	m_startPos = CIwFVec2(x, y);
	m_shootProb = 1.0f / 10.0f;
	m_shootFreq = 2.0f;
	m_shootCounter = 0.0f;
	m_shootFlag = false;
}

Alien::~Alien() {

}

void Alien::update(float dt) {
	m_t += dt;

	float xp = m_startPos.x - m_t * m_speed;
	float yp = -1 * m_amp * sin(m_freq * m_t) + m_startPos.y;

	float derivate = -1 * m_amp * cos(m_freq * m_t) * m_freq;

	float angle = atan2(derivate, 1);

	setPosition(xp, yp);

	m_shootCounter += dt;
	if (m_shootCounter >= 1.0f / m_shootFreq) {
		m_shootCounter = 0.0f;
		m_shootFlag = true;
	}
}

bool Alien::shouldShoot() {
	if (m_shootFlag && IwRandRange(100) < m_shootProb * 100) {
		m_shootFlag = false;
		return true;
	} else {
		m_shootFlag = false;
		return false;
	}
}

AlienSpawner::AlienSpawner(Animation *alienAnim, Animation *bulletAnim, float spawnRate) {
	m_alienAnim = alienAnim;
	m_bulletAnim = bulletAnim;
	m_time = 0.0f;
	m_spawnRate = spawnRate;
	m_spawning = true;

	m_bulletSpeed = (float)get_surf_width() / 1.6f;

}


AlienSpawner::~AlienSpawner()
{
	for (uint i = 0; i < m_bullets.size(); i++) {
		delete m_bullets[i];
	}
	for (uint i = 0; i < m_aliens.size(); i++) {
		delete m_aliens[i];
	}
}

template <typename T>
bool deleteAndRemoveIfEdge(T *elem) {
	if (elem->m_x + elem->m_w < 0) {
		CollisionManager::Remove(elem);
		delete elem;
		return true;
	}
	return false;
}

void AlienSpawner::update(float dt) {
	m_time += dt;
	if (m_time >= 1.0f / m_spawnRate) {
		m_time = 0;
		if (m_spawning) {
			spawn();
		}
	}
	// update aliens
	for (uint i = 0; i < m_aliens.size(); i++) {
		Alien* elem = m_aliens[i];
		elem->update(dt);
		if (elem->shouldShoot()) {
			// shoot bullet
			shootBullet(elem);

		}
	}
	// update bullets
	for (uint i = 0; i < m_bullets.size(); i++) {
		Sprite* elem = m_bullets[i];
		elem->translate(-m_bulletSpeed * dt, 0);
	}

	// check for aliens farther than left edge
	vector<Alien*>::iterator newend = remove_if(m_aliens.begin(), m_aliens.end(), deleteAndRemoveIfEdge<Alien>);
	m_aliens.erase(newend, m_aliens.end());
	// check for bullets farther than left edge
	vector<Sprite*>::iterator newend2 = remove_if(m_bullets.begin(), m_bullets.end(), deleteAndRemoveIfEdge<Sprite>);
	m_bullets.erase(newend2, m_bullets.end());


}


void AlienSpawner::spawn() {
	float posx = get_surf_width() + 100;
	float posy = IwRandRange(get_surf_height() - 20) + 20;
	float ampl = IwRandRange(get_surf_height() / 3.0f);
	float freq = 1.0f;
	float speed = get_surf_width() / ((IwRandRange(300) / 100.0f) + 2.0f);
	Alien* tmp = new Alien(posx, posy, m_alienAnim, ampl, freq, speed);
	CollisionManager::Add(tmp, "aliens");
	m_aliens.push_back(tmp);
}

void AlienSpawner::shootBullet(Alien* shooter) {
	Sprite* bullet = new Sprite(shooter->m_x, shooter->m_y, m_bulletAnim);
	CollisionManager::Add(bullet, "alien_bullets");
	m_bullets.push_back(bullet);
	//SoundPlayer::play("alien_shoot.wav"); // too much
}

void AlienSpawner::render() {
	for (uint i = 0; i < m_aliens.size(); i++) {
		m_aliens[i]->render();
	}
	for (uint i = 0; i < m_bullets.size(); i++) {
		m_bullets[i]->render();
	}

}
template<typename T>
class ifEqual {
public:
	ifEqual(T* elem) { m_elem = elem; }
	bool operator() (T* e) { return e == m_elem; }
	T* m_elem;
};

void AlienSpawner::destroyAlien(Node *alien) {
	vector<Alien*>::iterator newend = remove_if(m_aliens.begin(), m_aliens.end(),ifEqual<Node>(alien));
	CollisionManager::Remove(alien);
	delete alien;
	m_aliens.erase(newend, m_aliens.end());
}

void AlienSpawner::clear() {
	for (uint i = 0; i < m_bullets.size(); i++) {
		CollisionManager::Remove(m_bullets[i]);
		delete m_bullets[i];
	}
	for (uint i = 0; i < m_aliens.size(); i++) {
		CollisionManager::Remove(m_aliens[i]);
		delete m_aliens[i];
	}
	m_aliens.clear();
	m_bullets.clear();
}