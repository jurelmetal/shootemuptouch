#include "Node.h"


Node::Node() : 
m_x(0), m_y(0), m_w(0), m_h(0), m_rotation(0.0f)
{
	m_color.dword = 0xffffffffu;
	m_parent = NULL;
	m_updating = true;
}

Node::~Node()
{
	
}

bool Node::inside(uint x, uint y) {
	return (m_x <= x && x <= m_x + m_w) &&
		(m_y <= y && y <= m_y + m_h);
}
void Node::translate(float dx, float dy)
{
	m_x += dx;
	m_y += dy;
}

void Node::setColor(float r, float g, float b)
{
	m_color.r = r;
	m_color.g = g;
	m_color.b = b;
}

void Node::setColor(float r, float g, float b, float a)
{
	m_color.r = r;
	m_color.g = g;
	m_color.b = b;
	m_color.a = a;
}

void Node::setAlpha(float a)
{
	m_color.a = a;
}

void Node::setPosition(float x, float y)
{
	m_x = x;
	m_y = y;
}

void Node::setParent(Node* parent)
{
	m_parent = parent;
}

void Node::addChild(Node *child)
{
	child->setParent(this);
	m_children.push_back(child);
}

void Node::setRotation(float rads)
{
	m_rotation = rads;
}

void Node::update(float dt) {
	for (uint i = 0; i < m_children.size(); i++) {
		m_children[i]->update(dt);
	}
}